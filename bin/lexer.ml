type token =
  | MoveLeft
  | MoveRight
  | Incr
  | Decr
  | Output
  | Input
  | BracketOpen
  | BrackerClose
  | Comment of string
;;


let lex (code: string) : token list =
  let aux (acc: token list) (code_left: string) : token list =
    if String.length code_left = 0 then
      List.rev acc
    else
      []
  in
  aux [] code
;;