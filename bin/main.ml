let () =
  let m =
    Memory.create ()
    |> Memory.incr_cell
    |> Memory.incr_cell
    |> Memory.get_cell
  in
  print_int (Stdint.Int8.to_int m);
  assert (Array.length Sys.argv > 1);
  let file_name = Sys.argv.(0) in
  print_endline "Hello, brainfuck!";
  print_string file_name;
  print_newline ();;
