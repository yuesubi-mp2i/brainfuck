open Stdint;;


type cell = Int8.t;;
let cell_default_value : cell = Int8.of_int 0;;


type t = cell list * cell * cell list;;
exception No_memory_to_left;;


let create () : t =
  [], cell_default_value, []
;;


let get_cell (memory: t) : cell =
  let _, value, _ = memory in
  value
;;


let incr_cell (memory: t) : t =
  let left, value, right = memory in
  left, Int8.succ value, right
;;


let decr_cell (memory: t) : t =
  let left, value, right = memory in
  left, Int8.pred value, right
;;


let move_left (memory: t) : t =
  let left, value, right = memory in
  match left with
  | [] -> raise No_memory_to_left
  | new_value :: left_tail ->
    left_tail, new_value, (value :: right)
;;


let move_right (memory: t) : t =
  let left, value, right = memory in
  match right with
  | [] ->
    (value :: left), cell_default_value, []
  | new_value :: right_tail ->
    (value :: left), new_value, right_tail
;;